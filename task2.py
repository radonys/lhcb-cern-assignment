
""" Simple test on using mock
"""
import unittest
import mock

#########################

# function to test
def myFunction(objectIn):

    """ what you are supposed to test
    """
    return objectIn.aMethodToMock() + 2

#########################
#########################

# actual test
class ATestCase(unittest.TestCase):
    
    """ Base class for the test cases
    """

    def setUp(self):
        self.mockO = mock.MagicMock()
        self.mockO.aMethodToMock.return_value = 3 #Added line

    def tearDown(self):
        pass

class RealTestSuccess(ATestCase):

    """ The actual unit test
    """

    def test_myFunction(self):
        self.assertEqual(myFunction(self.mockO), 5)

if __name__ == '__main__':
    
    suite = unittest.defaultTestLoader.loadTestsFromTestCase( ATestCase )
    suite.addTest( unittest.defaultTestLoader.loadTestsFromTestCase( RealTestSuccess ) )
    testResult = unittest.TextTestRunner( verbosity = 2 ).run( suite )