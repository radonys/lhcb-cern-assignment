from elasticsearch import Elasticsearch
import unittest
import json

class task4(unittest.TestCase):
    
    def init(self):
        
        ES_HOST = {"host": "localhost", "port": 9200}
        self.es = Elasticsearch(hosts=[ES_HOST])

    def create(self):
        
        # create an index in elasticsearch, ignore status code 400 (index already exists)
        self.es.indices.create(index='my-index', ignore=400)
        #test whether file was actually created
        self.assertTrue(self.es.indices.exists('my-index'), "Index doesn't exists.")

    def list_delete(self):
        
        print [index for index in self.es.indices.get('*')]
        self.es.indices.delete('_all')
        self.assertFalse(self.es.indices.exists('my-index'), "Failed to delete Index.")

    def recreate_index(self):

        self.es.indices.create(index='my-index', ignore=400)

        current_process = {}
        file = open('task3.json','r')
        current_process.update(json.load(file))
        file.close()

        self.es.index(index='my-index', doc_type="currentinfo", body=current_process, id=1)

        resp = self.es.get(index='my-index', doc_type="currentinfo", id=1)
        retreive_data = resp["_source"]

        for index in retreive_data:
            
            self.assertEqual(retreive_data[index],current_process[index])

    def test_integration(self):
        
        self.init()
        self.create()
        self.list_delete()
        self.recreate_index()
        self.assertTrue(self.es.indices.exists('my-index'), "Index with current data doesn't exists.")

if __name__ == '__main__':
    unittest.main()
    