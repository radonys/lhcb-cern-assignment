# CERN LHCb Assignment

1. Create virtualenv by executing ```virtualenv -p python2 assignment```
2. Then, activate the environment using ```source assignment\bin\activate```
3. Execute ```pip install -r reequirements.txt```.
4. To execute each task, run the following command: ```python task[task number].py```.
5. To check using PyTest, run ```pytest task[1/2/4].py```.