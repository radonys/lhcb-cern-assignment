import psutil
import json

def currentProcess():

    current_process = {}

    p = psutil.Process() #Current process

    current_process['pid'] = p.pid
    current_process['name'] = p.name()
    current_process['status'] = p.status()
    current_process['threads'] = p.num_threads()

    file = open("task3.json",'w+')
    json.dump(current_process,file)
    file.close()
