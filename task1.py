import os
import tempfile
import unittest

class task1(unittest.TestCase):

    temp_filepath = os.path.join(tempfile.gettempdir(), "temp-testfile")

    #Create File
    def setUp(self):
        with open(self.temp_filepath, "wb") as f:
            f.write("Delete me!")

    def test_create(self):
        
        #test whether file was actually created
        self.assertTrue(os.path.isfile(self.temp_filepath), "File was not created.")

    def test_rm(self):
        
        # remove the file
        os.remove(self.temp_filepath)

        # test that it was actually removed
        self.assertFalse(os.path.isfile(self.temp_filepath), "Failed to remove the file.")

if __name__ == '__main__':
    unittest.main()